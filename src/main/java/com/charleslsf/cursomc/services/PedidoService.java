package com.charleslsf.cursomc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.charleslsf.cursomc.domain.Pedido;
import com.charleslsf.cursomc.repositories.PedidoRepository;
import com.charleslsf.cursomc.services.exception.ObjectNotFoundExceptions;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository repo;
	
	public Pedido find(Integer id) {
		Pedido obj = repo.findOne(id);
		if(obj == null) {
			throw new ObjectNotFoundExceptions("Objeto não encontrado! Id: " + id + ", Tipo " + Pedido.class.getName());
		}
		return obj;
	}
	
	public List<Pedido> findAll() {
		List<Pedido> list = repo.findAll();
		return list;
	}
}
