package com.charleslsf.cursomc.services;

import com.charleslsf.cursomc.domain.Cidade;
import com.charleslsf.cursomc.domain.Cliente;
import com.charleslsf.cursomc.domain.Endereco;
import com.charleslsf.cursomc.domain.enums.TipoCliente;
import com.charleslsf.cursomc.dto.ClienteDTO;
import com.charleslsf.cursomc.dto.ClienteNewDTO;
import com.charleslsf.cursomc.repositories.ClienteRepository;
import com.charleslsf.cursomc.repositories.EnderecoRepository;
import com.charleslsf.cursomc.services.exception.DataIntegrityExceptions;
import com.charleslsf.cursomc.services.exception.ObjectNotFoundExceptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {

	private ClienteRepository repo;

	private EnderecoRepository enderecoRepo;

	public Cliente find(Integer id) {
		Cliente obj = repo.findOne(id);
		if(obj == null) {
			throw new ObjectNotFoundExceptions("Objeto não encontrado! Id: " + id + ", Tipo " + Cliente.class.getName());
		}
		return obj;
	}

	public List<Cliente> findAll() {
        return repo.findAll();
	}

    public Cliente insert(Cliente obj) {
        obj.setId(null);
        obj = repo.save(obj);
        enderecoRepo.save(obj.getEnderecos());
        return obj;
    }

	public Cliente update(Cliente obj) {
		Cliente newObj = find(obj.getId());
		updateData(newObj, obj);
		return repo.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		try {
			repo.delete(id);
		}catch (DataIntegrityViolationException e) {
			throw new DataIntegrityExceptions("Não é possivel excluir porque há pedidos relacionados");
		}
	}

	public Page<Cliente> findPager(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = new PageRequest(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}

	public Cliente fromDTO(ClienteDTO objDto){
		return new Cliente(objDto.getId(), objDto.getNome(), objDto.getEmail(), null, null);
	}

	private void updateData(Cliente newObj, Cliente obj) {
		newObj.setNome(obj.getNome());
		newObj.setEmail(obj.getEmail());
	}

    public Cliente fromDTO(ClienteNewDTO objDto){
	    Cliente cli = new Cliente(null,objDto.getNome(), objDto.getEmail(), objDto.getCpfOuCnpj(), TipoCliente.toEnum(objDto.getTipo()));
        Cidade cid = new Cidade(objDto.getCidadeId(),null, null);
        Endereco end = new Endereco(null,objDto.getLogradouro(), objDto.getNumero(), objDto.getComplemento(), objDto.getBairro(), objDto.getCep(), cli, cid);
        cli.getEnderecos().add(end);
        cli.getTelefones().add(objDto.getTelefone1());
        if(objDto.getTelefone2() != null){
            cli.getTelefones().add(objDto.getTelefone2());
        }if(objDto.getTelefone3() != null){
            cli.getTelefones().add(objDto.getTelefone3());
        }
        return cli;
    }

	@Autowired
    public void setRepo(ClienteRepository repo) {
        this.repo = repo;
    }

    @Autowired
    public void setEnderecoRepo(EnderecoRepository enderecoRepo) {
        this.enderecoRepo = enderecoRepo;
    }
}
