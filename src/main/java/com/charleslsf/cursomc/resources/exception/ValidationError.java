package com.charleslsf.cursomc.resources.exception;

import java.util.ArrayList;
import java.util.List;

public class ValidationError extends StandardError {
    private static final long serialVersionUID = 1L;

    private List<FieldMessage> error = new ArrayList<>();

    public ValidationError(Integer status, String msg, Long timeStamp) {
        super(status, msg, timeStamp);
    }

    public void addError(String fildName,String messagem){
        error.add(new FieldMessage(fildName, messagem));
    }

    public List<FieldMessage> getError() {
        return error;
    }

}
